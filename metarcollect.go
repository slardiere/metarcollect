package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/eugecm/gometar/qnh"

	"github.com/pelletier/go-toml"

	"github.com/eugecm/gometar/metar/parser"
	"github.com/jlaffaye/ftp"

	"github.com/jackc/pgx"
)

const (
	METARINI = "metar.ini"
)

func getHR(tempAir int, tempPtRosee int) float64 {
	/* Based on https://fr.wikipedia.org/wiki/Point_de_ros%C3%A9e#Formule_de_Heinrich_Gustav_Magnus-Tetens */
	a := float64(17.27)
	b := float64(237.7)
	T := float64(tempAir)
	Tr := float64(tempPtRosee)
	num := a*Tr*(1-T/(b-T)) - (b * a * T / (b + T))
	den := b + Tr
	hr := math.Exp(num / den)
	return hr
}
func main() {
	var cfgToml *toml.Tree
	if _, err := os.Stat(METARINI); os.IsNotExist(err) {
		// No config file found at, using an empty one
		log.Fatalf("%s not found", METARINI)
	} else {
		cfgToml, err = toml.LoadFile(METARINI)
		if err != nil {
			log.Fatalf("Error opening %s : %s", METARINI, err.Error())
		}
	}

	pgDSN := cfgToml.Get("general.dsn").(string)
	pgWrite := cfgToml.Get("general.write").(bool)
	metarStation := cfgToml.Get("general.metar").(string)

	// ftp://tgftp.nws.noaa.gov/data/observations/metar/stations/LFBO.TXT"
	c, err := ftp.Dial("tgftp.nws.noaa.gov:21", ftp.DialWithTimeout(5*time.Second))
	if err != nil {
		log.Fatal(err)
	}

	err = c.Login("anonymous", "anonymous")
	if err != nil {
		log.Fatal(err)
	}
	metarURL := fmt.Sprintf("data/observations/metar/stations/%s.TXT", metarStation)
	r, err := c.Retr(metarURL)
	if err != nil {
		log.Panicf("file : %s not found : %s", metarURL, err.Error())
	}
	defer r.Close()

	buf, err := ioutil.ReadAll(r)

	if err := c.Quit(); err != nil {
		log.Fatal(err)
	}

	metardata := strings.Split(string(buf), "\n")

	var currentTimeAnalyze time.Time

	currentTimeAnalyze, err = time.Parse("2006/01/02 15:04", metardata[0])
	if err != nil {
		log.Printf("Cannot parse %s using current timestamp", metardata[0])
		currentTimeAnalyze = time.Now()
	}

	println(currentTimeAnalyze.Format(time.RFC3339))
	println(string(metardata[1]))
	p := parser.New()
	report, err := p.Parse(metardata[1])
	if err != nil {
		log.Fatal(err)
	}
	hr_pct := getHR(report.Temperature.Temperature, report.Temperature.DewPoint)
	fmt.Println("temperature")
	fmt.Printf("\t temperature : %d \n\t point de rosée : %d\n\t HR %d%%\n", report.Temperature.Temperature, report.Temperature.DewPoint, int(math.Round(hr_pct*100.0)))
	fmt.Println("weather")
	fmt.Printf("%v\n", report.Weather)
	fmt.Println("Vent")
	fmt.Printf("\t Vitesse : %d kt %f kmph %f mps",
		report.Wind.Speed.Speed,
		float64(report.Wind.Speed.Speed)*1.852,
		float64(report.Wind.Speed.Speed)*0.51444444)
	fmt.Printf(" Rafales : %d\n", report.Wind.Gust)
	fmt.Printf("\t Source : ")
	/*
		for (a,b) in [ (i - (360/32), i+(360/32))  for  i in [ i*(360/16) for i in range(0,16) ]]:
		     print(f"case float64(report.Wind.Source) > {a} && float64(report.Wind.Source) <= {b}:")

	*/
	dirText := "undefined"
	switch {
	case float64(report.Wind.Source) > 348.75 || float64(report.Wind.Source) <= 11.25:
		dirText = "N"
	case float64(report.Wind.Source) > 11.25 && float64(report.Wind.Source) <= 33.75:
		dirText = "NNE"
	case float64(report.Wind.Source) > 33.75 && float64(report.Wind.Source) <= 56.25:
		dirText = "NE"
	case float64(report.Wind.Source) > 56.25 && float64(report.Wind.Source) <= 78.75:
		dirText = "ENE"
	case float64(report.Wind.Source) > 78.75 && float64(report.Wind.Source) <= 101.25:
		dirText = "E"
	case float64(report.Wind.Source) > 101.25 && float64(report.Wind.Source) <= 123.75:
		dirText = "ESE"
	case float64(report.Wind.Source) > 123.75 && float64(report.Wind.Source) <= 146.25:
		dirText = "SE"
	case float64(report.Wind.Source) > 146.25 && float64(report.Wind.Source) <= 168.75:
		dirText = "SSE"
	case float64(report.Wind.Source) > 168.75 && float64(report.Wind.Source) <= 191.25:
		dirText = "S"
	case float64(report.Wind.Source) > 191.25 && float64(report.Wind.Source) <= 213.75:
		dirText = "SSO"
	case float64(report.Wind.Source) > 213.75 && float64(report.Wind.Source) <= 236.25:
		dirText = "SO"
	case float64(report.Wind.Source) > 236.25 && float64(report.Wind.Source) <= 258.75:
		dirText = "OSO"
	case float64(report.Wind.Source) > 258.75 && float64(report.Wind.Source) <= 281.25:
		dirText = "SO"
	case float64(report.Wind.Source) > 281.25 && float64(report.Wind.Source) <= 303.75:
		dirText = "ONO"
	case float64(report.Wind.Source) > 303.75 && float64(report.Wind.Source) <= 326.25:
		dirText = "NO"
	case float64(report.Wind.Source) > 326.25 && float64(report.Wind.Source) <= 348.75:
		dirText = "NNO"
	default:
		dirText = "undefined"
	}
	if report.Wind.Variable {
		fmt.Printf("in")
	}
	fmt.Printf("définie à %d° %s \n", report.Wind.Source, dirText)

	fmt.Println("Pression")

	pressionHpa, err := strconv.Atoi(report.Qnh.Pressure)

	if err != nil {
		log.Printf("Erreur decodage pression %s\n", report.Qnh.Pressure)
	} else {
		if report.Qnh.Unit == qnh.PressureUnitInchesOfMercury {
			pressionHpa = int(math.Round(float64(pressionHpa) * 33.8639))

		}
		fmt.Printf("\tPression : %d hPa", pressionHpa)
	}

	/**
	CREATE TABLE metar (
		refts timestamptz,
		metarsite text,
		temperature int,
		dew_point int,
		wind_speed_kmph float,
		wind_gust float,
		wind_source int,
		wind_source_text varchar(10),
		pressure_hpa int,
		hr_pct float,
		CONSTRAINT metar_pkey PRIMARY KEY (refts,metarsite)
	);
	*/
	if pgWrite {
		db, err := pgx.Connect(context.Background(), pgDSN)
		if err != nil {
			log.Fatal("Unable to connect PostgreSQL : ", err)
		}
		defer db.Close(context.Background())

		_, err = db.Exec(context.Background(), `INSERT INTO metar(
				refts, metarsite, 
				temperature, dew_point, 
				wind_speed_kmph, wind_gust, wind_source, wind_source_text, 
				pressure_hpa, hr_pct) 
				VALUES (
					$1, $2, 
					$3, $4, $5, $6, $7, $8,
					 $9, $10)`,
			currentTimeAnalyze,
			metarStation,
			report.Temperature.Temperature,
			report.Temperature.DewPoint,
			float64(report.Wind.Speed.Speed)*1.852,
			float64(report.Wind.Gust),
			report.Wind.Source,
			dirText,
			pressionHpa,
			(hr_pct * 100.0))

		if err != nil {
			log.Fatal("Failed to execute query: ", err)
		}
	}

}
